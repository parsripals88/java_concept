import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class IterableClass<Type> implements Iterable<Type>{

    private Type[] arrayList;
    private int size;

    public IterableClass(Type[] newArray) {
        this.arrayList = newArray;
        this.size = newArray.length;
    }

    @Override
    public Iterator iterator() {
        return new IteratorClass<Type>() ;
    }

    @Override
    public void forEach(Consumer action) {

    }

    @Override
    public Spliterator spliterator() {
        return null;
    }

    /**
     * Created by Palaniappan on 9/23/2016.
     */
    public class IteratorClass<Type> implements Iterator<Type> {

        private int cursor = 0;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public Type next() {
            if(cursor != size) {
                return (Type)arrayList[cursor++];
            } else {
                return null;
            }

        }

        @Override
        public void remove() {
            while(cursor < size) {
                arrayList[cursor] = arrayList[cursor+1];
                ++cursor;
            }
        }

        @Override
        public void forEachRemaining(Consumer action) {

        }
    }
}
