package pattern.decorator.examplethree;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class InputTest {

	@Test
	public void lowerCaseStreamTest() throws Exception{
		
		int c;
		String directory = System.getProperty("user.dir")
				+ "\\src\\pattern\\decorator\\examplethree\\test.txt";
			InputStream in = 
					new LowerCaseInputStream(
							new BufferedInputStream(
									new FileInputStream(directory)));
			
			while((c = in.read()) >= 0) {
				char ch = (char)c;
				System.out.print(ch);				
				assertTrue(Character.isLowerCase(ch) || Character.isSpaceChar(ch) || ch=='!');
			}
			
			in.close();
	}
}
