package core.MathClass;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class MathFunctions {

    public int abs(int a) {
        return Math.abs(a);
    }

    public double ceil(double a) {
        return Math.ceil(a);
    }

    public double floor(double a) {
        return Math.floor(a);
    }

    public int floorDiv(int a, int b) {
        return Math.floorDiv(a, b);
    }

    public int min(int a, int b) {
        return Math.min(a, b);
    }

    public int max(int a, int b) {
        return Math.max(a, b);
    }

    public double round(double a) {
        return Math.round(a);
    }

    public double random() {
        return Math.random();
    }

}
