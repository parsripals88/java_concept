package core.MathClass;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class MathOperator {

    public int addition(int a , int b) {
        return a + b;
    }

    public int subtraction(int a , int b) {
        return a - b;
    }

    public int multiplication(int a , int b) {
        return a * b;
    }

    public int divisionReturnInteger(int a , int b) {
        return a / b;
    }

    public double divisionReturnDouble(int a , int b) {
        return a / b;
    }

    /*Floating point precision is not precise*/
    public void floatingPointPrecisionByAdding100Times(double resultDbl3) {
        for(int i=0; i<100; i++){
            resultDbl3 += 0.01D;
        }
    }
}
