package core.datatypes;

import static org.junit.Assert.*;

import org.junit.Test;

public class TwoDimensionalArrayClass {
	//2d array is an array of arrays
	//length of an array is established when the array is created
	//after creation, length is fixed
	
	//in actual implementation, it is not a matrix
	//collection of small single dimensional arrays
	//whatever is your row size, you create as many single dimensional arrays
	
	int[][] myArray = new int[3][2];
	//we have 3 single dimensional arrays created
	
	@Test
	public void checkInitialArrayContents() {
		//array starts with zero indexing
		assertEquals(myArray[1][1], 0);
	}
	
	@Test
	public void createArrayonFly() {
		int[][] intArray = {{1, 2}, {3, 4}};
		assertEquals(intArray[1][1], 4);
	}
	
	@Test
	public void createArrayWithNewKeywordAndDirectInitialization() {
		int[] marks[] = new int[][]{{89, 98}, {97, 89, 99}};
		assertEquals(marks[1][0], 97);
	}
	
	@Test
	public void createArrayWithNewKeywordAndSubsequentInitialization() {
		int marks[][] = new int[5][4];
		for(int i=0; i<marks.length; i++)
			for(int j=0; j<marks[i].length; j++)
				marks[i][j] = 100 - i;
		assertEquals(marks[2][0], 98);
	}
	
	@Test
	public void createArrayOfDifferentSizes() {
		//you cannot skip the row when creation
		//called as parse matrix
		int[][] marks = new int[5][];
		assertNull(marks[0]);
		marks[0] = new int[2];
		marks[0][1] = 2;
		marks[1] = new int[10];
		assertEquals(marks[1][3], 0);
		assertNotNull(marks[0]);
	}
	
	@Test
	public void arrayLength() {
		int marks[][] = new int[5][2];
		assertEquals(marks.length, 5);
	}
	
	@Test
	public void arrayDefaultValues() {
		int[] marks = new int[5];
		assertEquals(marks[0], 0);
	}
}
