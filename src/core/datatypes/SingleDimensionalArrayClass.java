package core.datatypes;

import org.junit.Test;
import static org.junit.Assert.*;

public class SingleDimensionalArrayClass {
	//Array is used to store a collection of data
	//Length of an array is established when the array is created.
	//After creation, its length is fixed.
	
	//when we say new keyword, data goes into heap
	int[] myArray = new int[5];
	//for this five location with be created in heap
	//myArray will be created in stack and loaded with a address which points to the heap locations
	//myArray (address) in stack --> first location of five elements
	
	@Test
	public void checkInitialArrayContents() {
		//array starts with zero indexing
		assertEquals(myArray[3], 0);
		//myArray will contain an address
		assertNotNull(myArray);
	}
	
	@Test
	public void createArrayonFly() {
		int[] intArray = {1, 2, 3, 4};
		assertEquals(intArray[3], 4);
	}
	
	@Test
	public void createArrayWithNewKeywordAndDirectInitialization() {
		int[] marks = new int[]{89, 98, 97, 89, 99};
		assertEquals(marks[2], 97);
	}
	
	@Test
	public void createArrayWithNewKeywordAndSubsequentInitialization() {
		int[] marks = new int[5];
		for(int i=0; i<marks.length; i++)
			marks[i] = 100 - i;
		assertEquals(marks[2], 98);
	}
	
	@Test
	public void arrayLength() {
		int[] marks = new int[5];
		assertEquals(marks.length, 5);
	}
	
	@Test
	public void arrayDefaultValues() {
		int[] marks = new int[5];
		assertEquals(marks[0], 0);
	}
	
	
	
}
