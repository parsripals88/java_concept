package adapter.example2.MediaPlayerAdvanced;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class VlcPlayer implements AdvancedMediaPlayer  {

    @Override
    public String play(String fileName) {
        return "Playing vlc file. Name: "+ fileName;
    }
}
