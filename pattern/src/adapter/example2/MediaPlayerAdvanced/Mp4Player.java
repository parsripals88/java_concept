package adapter.example2.MediaPlayerAdvanced;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class Mp4Player implements AdvancedMediaPlayer {

    @Override
    public String play(String fileName) {
        return "Playing mp4 file. Name: "+ fileName;
    }
}
