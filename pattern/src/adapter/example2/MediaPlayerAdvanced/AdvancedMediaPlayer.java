package adapter.example2.MediaPlayerAdvanced;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public interface AdvancedMediaPlayer {
    public String play(String fileName);
}
