package adapter.example2.MediaPlayer;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class AudioPlayer implements MediaPlayer {
    @Override
    public String play(String audioType, String fileName) {
        //inbuilt support to play mp3 music files
        if(audioType.equalsIgnoreCase("mp3")){
            return "Playing mp3 file. Name: " + fileName;
        }

        //mediaAdapter is providing support to play other file formats
        if(audioType.equalsIgnoreCase("vlc") || audioType.equalsIgnoreCase("mp4")){
            MediaAdapter mediaAdapter = new MediaAdapter(audioType);
            return mediaAdapter.play(audioType, fileName);
        }

        return "Invalid media. " + audioType + " format not supported";
    }
}
