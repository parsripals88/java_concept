package adapter.example2.MediaPlayer;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public interface MediaPlayer {

    public String play(String audioType, String fileName);


}
