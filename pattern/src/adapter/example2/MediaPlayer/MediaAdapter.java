package adapter.example2.MediaPlayer;

import adapter.example2.MediaPlayerAdvanced.AdvancedMediaPlayer;
import adapter.example2.MediaPlayerAdvanced.Mp4Player;
import adapter.example2.MediaPlayerAdvanced.VlcPlayer;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class MediaAdapter implements MediaPlayer {

    AdvancedMediaPlayer advancedMusicPlayer;

    public MediaAdapter(String audioType){

        if(audioType.equalsIgnoreCase("vlc") ){
            advancedMusicPlayer = new VlcPlayer();

        }else if (audioType.equalsIgnoreCase("mp4")){
            advancedMusicPlayer = new Mp4Player();
        }
    }

    @Override
    public String play(String audioType, String fileName) {
        if(audioType.equalsIgnoreCase("vlc")){
            return advancedMusicPlayer.play(fileName);
        } else if(audioType.equalsIgnoreCase("mp4")){
            return advancedMusicPlayer.play(fileName);
        } else{
            return "";
        }
    }
}
