package adapter.example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class CelciusThermometer {

    double temp;

    public CelciusThermometer(double temp) {
        this.temp = temp;
    }

    public double getTemperateInC() {
        return temp;
    }

}
