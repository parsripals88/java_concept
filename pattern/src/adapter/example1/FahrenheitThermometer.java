package adapter.example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public interface FahrenheitThermometer {

    double getTemperateInF();

}
