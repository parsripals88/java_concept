package example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class MySQLRegistrationDAOFactory {
    public RegistrationDAO create() {
        return new MySQLRegistrationDAO();
    }
}
