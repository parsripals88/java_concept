package example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public interface RegistrationDAOFactory {
    public RegistrationDAO create();
}
