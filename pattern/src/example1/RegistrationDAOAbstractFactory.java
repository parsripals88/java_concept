package example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public abstract class RegistrationDAOAbstractFactory {

    public static RegistrationDAOFactory createRegistrationDAO(DAOType daoType) {
        if (daoType == DAOType.MYSQL) {
            return new MySQLRegistrationDAOFactory();
        } else {
            return new OracleRegistrationDAOFactory();
        }
    }

}
