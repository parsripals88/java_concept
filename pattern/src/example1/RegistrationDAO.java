package example1;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public abstract class RegistrationDAO {
    public abstract void persist(Registration registration);

    public abstract void update(Registration registration);
}
