package observer.example1.observer;

import observer.example1.subject.WeatherData;

public class StatisticsDisplay implements Observer, DisplayElement {

	private float averageTemperature;
	private float minimumTemperature;
	private float maximumTemperature;
	private WeatherData weatherData;
	
	public StatisticsDisplay(WeatherData weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}

	@Override
	public String display() {
		return "Avg/Max/Min temperature = "+
				averageTemperature + "/" +
				maximumTemperature + "/" +
				minimumTemperature;
	}

	@Override
	public void update(float temp, float humidity, float pressure) {
		this.averageTemperature = temp;
		this.minimumTemperature = temp;
		this.maximumTemperature = temp;
	}

}
