package observer.example1.observer;

import observer.example1.subject.WeatherData;

public class ForecastDisplay implements Observer, DisplayElement{

	private String forecast;
	private WeatherData weatherData;

	public ForecastDisplay(WeatherData weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}
	
	@Override
	public String display() {
		return "Forecast: "+ forecast;
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		if(temperature >= 82) {
			forecast = "Watch out for cooler, rainy weather";
		} else if(temperature >= 80) {
			forecast = "Improving weather on the way!";
		} else {
			forecast = "More of the same";
		}
		
	}

}
