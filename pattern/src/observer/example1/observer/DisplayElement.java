package observer.example1.observer;

public interface DisplayElement {
	public String display();
}
