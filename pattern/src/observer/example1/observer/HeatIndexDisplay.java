package observer.example1.observer;

import observer.example1.subject.Subject;

public class HeatIndexDisplay implements Observer, DisplayElement{

	float heatIndex;
	private Subject weatherData;
	
	public HeatIndexDisplay(Subject weatherData) {
		this.weatherData = weatherData;
		weatherData.registerObserver(this);
	}
	
	@Override
	public String display() {
		return "Heat index is " + heatIndex;
	}

	@Override
	public void update(float temperature, float humidity, float pressure) {
		heatIndex = (float) (16.923 
				+ 1.85212 * 0.1 * temperature
				+ 5.37941 * humidity 
				- 1.00254 * 0.1 * temperature * humidity 
				+ 9.41695 * .001 * temperature * temperature
				+ 7.28898 * 0.001 * humidity * humidity
				+ 3.45372 * 0.0001 * temperature * temperature * humidity
				- 8.14971 * 0.0001 * temperature * humidity * humidity
				+ 1.02102 * 0.00001 * temperature * temperature * humidity * humidity
				- 3.8646 * 0.000001 * temperature * temperature * temperature
				+ 2.91583 * 0.00001 * humidity * humidity * humidity
				+ 1.42721 * 0.000001 * temperature * temperature * temperature * humidity
				+ 1.97483 * 0.00000001 * temperature * humidity * humidity * humidity
				- 2.18429 * 0.0000000001 * humidity * humidity
				+ 8.43296 * 0.000000000001 * temperature * temperature
				- 4.81975 * 0.000000000000001 * temperature * temperature * temperature * humidity * humidity * humidity);
		
	}

}
