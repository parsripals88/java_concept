package observer.example1.subject;

import observer.example1.observer.Observer;

public interface Subject{
	public void registerObserver(Observer o);
	public void removeObserver(Observer o);
	public void notifyObservers();
}
