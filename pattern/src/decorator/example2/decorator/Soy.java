package decorator.example2.decorator;

import decorator.example2.component.Beverage;

public class Soy extends CondimentDecorator {

	Beverage beverage;
	
	public Soy(Beverage beverage) {
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return beverage.getDescription() + ", Soy";
	}

	@Override
	public double cost() {
		float cost = 0f;
		if(getSize() == Size.TALL) {
			cost = 0.10f;
		} else if(getSize() == Size.GRANDE) {
			cost = 0.15f;
		} else {
			cost = 0.20f;
		}
		return beverage.cost() + cost;
	}

}
