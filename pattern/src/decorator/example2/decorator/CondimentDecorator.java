package decorator.example2.decorator;

import decorator.example2.component.Beverage;

public abstract class CondimentDecorator extends Beverage {

	public abstract String getDescription();
	
}
