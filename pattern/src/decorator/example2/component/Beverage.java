package decorator.example2.component;

public abstract class Beverage {
	
	public enum Size {TALL, GRANDE, VENTI};
	public Size size = Size.TALL;
	
	String description = "Unknown Beverage";
	
	public String getDescription() {
		return description;
	}
	
	public abstract double cost();
	
	public void setSize(Size size){
		this.size = size;
	}
	
	public Size getSize() {
		return this.size;
	}
}
