package decorator.example1.decorator;

import decorator.example1.component.Beverage;

public class Whip extends Beverage {

	Beverage beverage;
	
	public Whip(Beverage beverage) {
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return beverage.getDescription() + ", Whip";
	}

	@Override
	public double cost() {
		return beverage.cost() + 0.10;
	}

}
