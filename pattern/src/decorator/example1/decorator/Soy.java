package decorator.example1.decorator;

import decorator.example1.component.Beverage;

public class Soy extends Beverage {

	Beverage beverage;
	
	public Soy(Beverage beverage) {
		this.beverage = beverage;
	}
	
	@Override
	public String getDescription() {
		return beverage.getDescription() + ", Soy";
	}

	@Override
	public double cost() {
		return beverage.cost() + 0.12;
	}

}
