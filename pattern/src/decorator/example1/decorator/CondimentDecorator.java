package decorator.example1.decorator;

import decorator.example1.component.Beverage;

public abstract class CondimentDecorator extends Beverage {

	public abstract String getDescription();
}
