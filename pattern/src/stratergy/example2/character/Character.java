package stratergy.example2.character;

import stratergy.example2.weapon.WeaponBehavior;

public abstract class Character {
	WeaponBehavior weapon;
	abstract void fight();
	
	void setWeapon(WeaponBehavior w) {
		weapon = w;
	}
}
