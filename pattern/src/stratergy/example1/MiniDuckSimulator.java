package stratergy.example1;

import org.junit.Test;

import stratergy.example1.duck.Duck;
import stratergy.example1.duck.MallardDuck;
import stratergy.example1.duck.ModelDuck;
import stratergy.example1.fly.FlyRocketPowered;

import static org.junit.Assert.*;
import static org.hamcrest.core.Is.is;

public class MiniDuckSimulator {

	@Test
	public void simulate() {
		Duck mallard = new MallardDuck();
		Assert.assertThat(mallard.performQuack(), Is.is("Quack"));
		Assert.assertThat(mallard.performFly(), Is.is("I'm flying!!!"));
	}
	
	@Test
	public void simulateDynamicBehavior() {
		Duck model = new ModelDuck();
		Assert.assertThat(model.performFly(), Is.is("I can't fly"));
		model.setFlyBehavior(new FlyRocketPowered());
		Assert.assertThat(model.performFly(), Is.is("I'm flying with a rocket!"));
	}

}
