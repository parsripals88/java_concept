package stratergy.example1.duck;

import stratergy.example1.fly.FlyBehavior;
import stratergy.example1.quack.QuackBehavior;

public abstract class Duck {

	FlyBehavior flyBehavior;
	QuackBehavior quackBehavior;
	
	public Duck() {
		
	}
	
	public abstract String display();
	
	public void setFlyBehavior(FlyBehavior fb) {
		flyBehavior = fb;
	}
	
	public String  performFly() {
		return flyBehavior.fly();
	}
	
	public void setQuackBehavior(QuackBehavior qb) {
		quackBehavior = qb;
	}
	
	public String performQuack() {
		return quackBehavior.quack();
	}
	
	public String swim() {
		return "All ducks float; even decoy!";
	}
}
