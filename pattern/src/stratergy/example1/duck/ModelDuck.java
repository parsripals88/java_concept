package stratergy.example1.duck;

import stratergy.example1.fly.FlyNoWay;
import stratergy.example1.quack.Quack;

public class ModelDuck extends Duck {

	public ModelDuck() {
		flyBehavior = new FlyNoWay();
		quackBehavior = new Quack();
	}
	
	@Override
	public String display() {
		return "I'm a model duck";
	}

}
