package stratergy.example1.duck;

import stratergy.example1.fly.FlyWithWings;
import stratergy.example1.quack.Quack;

public class MallardDuck extends Duck {

	public MallardDuck() {
		quackBehavior = new Quack();
		flyBehavior = new FlyWithWings();
	}
	
	@Override
	public String display() {
		return "I'm a real Mallard duck";
	}

}
