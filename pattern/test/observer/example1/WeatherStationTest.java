package observer.example1;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import pattern.observer.example1.observer.CurrentConditionsDisplay;
import pattern.observer.example1.observer.ForecastDisplay;
import pattern.observer.example1.observer.HeatIndexDisplay;
import pattern.observer.example1.observer.StatisticsDisplay;
import pattern.observer.example1.subject.WeatherData;

public class WeatherStationTest {

	@Test
	public void simulateObserver() {
		WeatherData weatherData = new WeatherData();
		
		CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);
		StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
		ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);
		HeatIndexDisplay heatindexDisplay = new HeatIndexDisplay(weatherData);
		
		weatherData.setMeasurements(80, 65, 30.4f);
		assertThat(currentDisplay.display(), is("Current conditions : 80.0F degrees and 65.0% humidity"));
		assertThat(statisticsDisplay.display(), is("Avg/Max/Min temperature = 80.0/80.0/80.0"));
		assertThat(forecastDisplay.display(), is("Forecast: Improving weather on the way!"));
		assertThat(heatindexDisplay.display(), is("Heat index is"));
		
		weatherData.setMeasurements(82, 70, 29.2f);
		assertThat(currentDisplay.display(), is("Current conditions : 82.0F degrees and 70.0% humidity"));
		assertThat(statisticsDisplay.display(), is("Avg/Max/Min temperature = 82.0/82.0/82.0"));
		assertThat(forecastDisplay.display(), is("Forecast: Watch out for cooler, rainy weather"));
		assertThat(heatindexDisplay.display(), is("Heat index is"));
		
		weatherData.setMeasurements(78, 90, 29.2f);
		assertThat(currentDisplay.display(), is("Current conditions : 78.0F degrees and 90.0% humidity"));
		assertThat(statisticsDisplay.display(), is("Avg/Max/Min temperature = 78.0/78.0/78.0"));
		assertThat(forecastDisplay.display(), is("Forecast: More of the same"));
		assertThat(heatindexDisplay.display(), is("Heat index is"));
	}
}
