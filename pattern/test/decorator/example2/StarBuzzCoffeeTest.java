package decorator.example2;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pattern.decorator.example2.component.Beverage;
import pattern.decorator.example2.component.DarkRoast;
import pattern.decorator.example2.component.Espresso;
import pattern.decorator.example2.component.HouseBlend;
import pattern.decorator.example2.component.Beverage.Size;
import pattern.decorator.example2.decorator.Mocha;
import pattern.decorator.example2.decorator.Soy;
import pattern.decorator.example2.decorator.Whip;

public class StarBuzzCoffeeTest {

	@Test
	public void createExpresso() {
		Beverage beverage = new Espresso();
		assertEquals("Espresso", beverage.getDescription());
		assertEquals(1.99f, beverage.cost(), 0.1);
	}
	
	@Test
	public void createDarkRoastWith2MochaandWhip() {
		Beverage beverage2 = new DarkRoast();
		beverage2 = new Mocha(beverage2);
		beverage2 = new Mocha(beverage2);
		beverage2 = new Whip(beverage2);
		assertEquals("Dark Roast, Mocha, Mocha, Whip", beverage2.getDescription());
		assertEquals(1.49f, beverage2.cost(), 0.01);
	}
	
	@Test
	public void createHouseBlendWithSoyAndMocha() {
		Beverage houseBlend = new HouseBlend();	
		houseBlend.setSize(Size.VENTI);
		assertEquals(houseBlend.getSize(), Size.VENTI);
		Beverage houseBlendSoy = new Soy(houseBlend);	
		houseBlendSoy.setSize(Size.TALL);
		assertEquals(houseBlend.getSize(), Size.VENTI);
		Beverage houseBlendSoyMocha = new Mocha(houseBlendSoy);
		Beverage houseBlendSoyMochaWhip = new Whip(houseBlendSoyMocha);
		assertEquals("House Blend, Soy, Mocha, Whip", houseBlendSoyMochaWhip.getDescription());
		assertEquals(1.29f, houseBlendSoyMochaWhip.cost(), 0.01);
	}
	
}
