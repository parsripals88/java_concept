package decorator.example1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pattern.decorator.example1.component.Beverage;
import pattern.decorator.example1.component.DarkRoast;
import pattern.decorator.example1.component.Espresso;
import pattern.decorator.example1.component.HouseBlend;
import pattern.decorator.example1.decorator.Mocha;
import pattern.decorator.example1.decorator.Soy;
import pattern.decorator.example1.decorator.Whip;

public class StarBuzzCoffeeTest {

	@Test
	public void createExpresso() {
		Beverage beverage = new Espresso();
		assertEquals(beverage.getDescription(), "Espresso");
		assertEquals(beverage.cost(), 1.99f, 0.1);
	}
	
	@Test
	public void createDarkRoastWith2MochaandWhip() {
		Beverage beverage2 = new DarkRoast();
		beverage2 = new Mocha(beverage2);
		beverage2 = new Mocha(beverage2);
		beverage2 = new Whip(beverage2);
		assertEquals(beverage2.getDescription(), "Dark Roast, Mocha, Mocha, Whip");
		assertEquals(beverage2.cost(), 1.49f, 0.1);
	}
	
	@Test
	public void createHouseBlendWithSoyAndMocha() {
		Beverage beverage3 = new HouseBlend();
		beverage3 = new Soy(beverage3);
		beverage3 = new Mocha(beverage3);
		beverage3 = new Whip(beverage3);
		assertEquals(beverage3.getDescription(), "House Blend, Soy, Mocha, Whip");
		assertEquals(beverage3.cost(), 1.34f, 0.1);
	}
	
}
