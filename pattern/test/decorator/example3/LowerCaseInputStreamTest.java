package decorator.example3;

import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class LowerCaseInputStreamTest {

    @Test
    public void lowerCaseStreamTest() throws Exception{

        int c;
        String directory = System.getProperty("user.dir")
                + "\\test\\pattern\\decorator\\examplethree\\test.txt";
        InputStream in =
                new LowerCaseInputStream(
                        new BufferedInputStream(
                                new FileInputStream(directory)));

        while((c = in.read()) >= 0) {
            char ch = (char)c;
            System.out.print(ch);
            assertTrue(Character.isLowerCase(ch) || Character.isSpaceChar(ch) || ch=='!');
        }

        in.close();
    }
}