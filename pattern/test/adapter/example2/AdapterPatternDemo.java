package adapter.example2;

import org.junit.Assert;
import org.junit.Test;
import pattern.adapter.example2.MediaPlayer.AudioPlayer;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class AdapterPatternDemo {

    @Test
    public void demo() {
        AudioPlayer audioPlayer = new AudioPlayer();
        Assert.assertEquals("Playing mp3 file. Name: beyond the horizon.mp3",
                audioPlayer.play("mp3", "beyond the horizon.mp3"));
        Assert.assertEquals("Playing mp4 file. Name: alone.mp4",
                audioPlayer.play("mp4", "alone.mp4"));
        Assert.assertEquals("Playing vlc file. Name: far far away.vlc",
                audioPlayer.play("vlc", "far far away.vlc"));
        Assert.assertEquals("Invalid media. avi format not supported", audioPlayer.play("avi", "mind me.avi"));
    }
}
