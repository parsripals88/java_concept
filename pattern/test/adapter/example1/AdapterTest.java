package adapter.example1;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Palaniappan on 10/12/2016.
 */
public class AdapterTest {

    @Test
    public void demostrateAdapter() {
        CelciusThermometer celciusThermometer = new CelciusThermometer(34);
        FahrenheitThermometer thermometer
                = new CelciusToFahrenheitAdapter(celciusThermometer);

        Assert.assertEquals("Celcius", 34, celciusThermometer.getTemperateInC(), 0.01);
        Assert.assertEquals(" Fahrenheit: ", 93.2, thermometer.getTemperateInF(), 0.01);
    }
}
