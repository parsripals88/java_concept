package polymorphism;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

class Calculator {
	
	//core.polymorphism - object ability to behave differently
	//Method core.overloading is a way to achieve core.polymorphism
	//same method name to denote several different operations
	
	//two or more methods in a java class can have the same name,
		// if the argument lists are different
	//Argument list could differ in
		//No of parameters
		//Data type of parameters
		//Sequence of parameters
	//similarly constructors can also be overloaded
	int add(int a, int b) {
		return a+b;
	}
	
	int add(int a, int b, int c){
		return a+b+c;
	}
	
	float add(float a, float b) {
		return a+b;
	}
	
	float add(int a, float b) {
		return a+b;
	}
	
	float add(float a, int b) {
		return a+b;
	}
	
	/* Not allowed since it has the same parameters to
	 * 	int add(int a, int b) even when the return argument is different 
	   float add(int a, int b){
		return a+b;
	}*/
}

public class MethodOverloading {

	@Test
	public void testMethodOverloading() {
		Calculator obj = new Calculator();
		Assert.assertEquals(obj.add(18, 19), 37);
		Assert.assertEquals(obj.add(4, 6.0f), 10.0, 0);
		Assert.assertEquals(obj.add(4.0f, 6.0f), 10.0, 0);
	}
	
}
