package polymorphism;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

class Student {
	
	private int age;
	private String name;
	
	public Student(int age, String name) {
		this.age = age;
		this.name = name;
	}
	
	public Student(String name) {
		this.name = name;
	}
	
	public Student() {
		//should be the first line in a core.constructor
		this(0, null); 
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

public class ConstructorOverloading {
	
	@Test
	public void testConstructorOverloading() {
		Student obj = new Student("name");
		Assert.assertEquals(obj.getAge(), 0);
		Assert.assertEquals(obj.getName(), "name");
		
		obj = new Student();
		Assert.assertEquals(obj.getName(), null);
	}
}
