package datatypes;

public class PrimitiveClass {
	
	//integer data types
	byte byteType; //1 byte
	int intType; //2 bytes
	short shortType; //4 bytes
	long longType; //8 bytes
	
	//floating data types
	float floatType; //4 bytes
	double doubleType; //8 bytes
	
	//textual data types
	char charType; //2 byte
	
	//Logical Data Type true/false
	boolean booleanType; //1 byte
}
