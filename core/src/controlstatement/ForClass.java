package controlstatement;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ForClass {

	@Test
	public void forStatement() {
		List<Integer> storage = new ArrayList<Integer>();
		for(int i=0; i<10; i++) {
			storage.add(i);
		}
		Assert.assertEquals(storage.contains(3), true);
	}
	
}
