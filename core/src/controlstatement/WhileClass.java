package controlstatement;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class WhileClass {

	@Test
	public void whileStatement() {
		List<Integer> storage = new ArrayList<Integer>();
		int i=0;
		while(i < 10) {
			storage.add(i);
			i = i+1;
		}
		Assert.assertEquals(storage.contains(3), true);
	}
	
}
