package controlstatement;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class IfElseClass {

	@Test
	public void ifElseStatement() {
		int no =10;
		boolean validScore;
		if(no < 0) {
			validScore = false;
		} else if(no > 100){
			validScore = false;
		} else {
			validScore = true;
		}
		Assert.assertEquals(validScore, true);
	}
	
}
