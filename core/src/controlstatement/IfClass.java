package controlstatement;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class IfClass {

	@Test
	public void ifStatement() {
		int no =10;
		boolean even;
		if(no%2==0) {
			even = true;
		} else {
			even = false;
		}
		Assert.assertEquals(even, true);
	}
	
}
