package controlstatement;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

public class SwitchClass {

	@Test
	public void switchStatement() {
		Assert.assertEquals(withDefault('d'), "Go right");
		Assert.assertEquals(withDefault('x'), "Do nothing");
		Assert.assertEquals(withoutDefault('x'), "Do nothing");
		Assert.assertEquals(noBreak('d'), "Do nothing");
	}
	
	public String withDefault(char c) {
		String instruction;
		switch(c) {
			case 'w': instruction = "Go up"; break; 
			case 'a': instruction = "Go left"; break;
			case 'd': instruction = "Go right"; break;
			case 's': instruction = "Go down"; break;
			default: instruction = "Do nothing"; break;
		}
		return instruction;
	}
	
	public String withoutDefault(char c) {
		String instruction = "Do nothing";
		switch(c) {
			case 'w': instruction = "Go up"; break; 
			case 'a': instruction = "Go left"; break;
			case 'd': instruction = "Go right"; break;
			case 's': instruction = "Go down"; break;
		}
		return instruction;
	}
	
	public String noBreak(char c) {
		String instruction = "Do nothing";
		switch(c) {
			case 'w': instruction = "Go up"; 
			case 'a': instruction = "Go left"; 
			case 'd': instruction = "Go right"; 
			case 's': instruction = "Go down"; 
			default: instruction = "Do nothing";
		}
		return instruction;
	}
	
}
