package Strings;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class regex {

	@Test
	public void oneOrMoreSpaces() {
		String text = "Hello  hello?";
		String[] words = text.split(" +");
		Assert.assertArrayEquals(words, new String[]{"Hello", "hello?"});
	}
	
	public static List<String> getTokens(String text, String pattern) {
		ArrayList<String> tokens = new ArrayList<String>();
		Pattern tokSplitter = Pattern.compile(pattern);
		Matcher m = tokSplitter.matcher(text);
		
		while (m.find()) { //Attempts to find the next subsequence of the input sequence that matches the pattern.
			tokens.add(m.group()); //Returns the input subsequence matched by the previous match.
		}
		
		return tokens;
	}
	
	@Test
	public void concatenation() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "it");
		Assert.assertEquals(words.size(), 2);
		Assert.assertEquals(words.get(0), "it");
		Assert.assertEquals(words.get(1), "it");
		
	}
	
	@Test
	public void oneOrMore() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "it+");//zero or one
		Assert.assertEquals(words.size(), 2);
		Assert.assertEquals(words.get(0), "itt");
		Assert.assertEquals(words.get(1), "it");
	}
	
	
	@Test
	public void grouping() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "i(t+)");
		Assert.assertEquals(words.size(), 2);
		Assert.assertEquals(words.get(0), "itt");
		Assert.assertEquals(words.get(1), "it");
	}
	
	@Test
	public void zeroOrMore() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "it*");
		Assert.assertEquals(words.size(), 5);
		Assert.assertEquals(words.get(0), "itt");
		Assert.assertEquals(words.get(1), "i");
		Assert.assertEquals(words.get(2), "i");
		Assert.assertEquals(words.get(3), "it");
		Assert.assertEquals(words.get(4), "i");
	}
	
	@Test
	public void alternation() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "it|st");
		Assert.assertEquals(words.size(), 3);
		Assert.assertEquals(words.get(0), "it");
		Assert.assertEquals(words.get(1), "st");
	}
	
	@Test
	public void characterClasses() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "[123]");
		Assert.assertEquals(words.size(), 4);
		Assert.assertEquals(words.get(0), "1");
		Assert.assertEquals(words.get(1), "2");
		Assert.assertEquals(words.get(2), "3");
		Assert.assertEquals(words.get(3), "3");
	}
	
	@Test
	public void characterRanges() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "[1-3]");
		Assert.assertEquals(words.size(), 4);
		Assert.assertEquals(words.get(0), "1");
		Assert.assertEquals(words.get(1), "2");
		Assert.assertEquals(words.get(2), "3");
		Assert.assertEquals(words.get(3), "3");
		words = getTokens(text, "[a-f]");
		Assert.assertEquals(words.size(), 5);
		Assert.assertEquals(words.get(0), "a");
		Assert.assertEquals(words.get(1), "a");
		Assert.assertEquals(words.get(2), "e");
		Assert.assertEquals(words.get(3), "a");
		Assert.assertEquals(words.get(4), "a");
	}
	
	@Test
	public void NotCharacterRanges() {
		String text = "Splitting a string, it's as easy as 1 2 33! Right?";
		List<String> words = getTokens(text, "[^a-z123]");
		Assert.assertEquals(words.size(), 16);
		Assert.assertEquals(words.get(0), "S");
		Assert.assertEquals(words.get(1), " ");
		Assert.assertEquals(words.get(2), " ");
		Assert.assertEquals(words.get(3), ",");
		Assert.assertEquals(words.get(4), " ");
		Assert.assertEquals(words.get(5), "'");
		Assert.assertEquals(words.get(6), " ");
		Assert.assertEquals(words.get(7), " ");
		Assert.assertEquals(words.get(8), " ");
		Assert.assertEquals(words.get(9), " ");
		Assert.assertEquals(words.get(10), " ");
		Assert.assertEquals(words.get(11), " ");
		Assert.assertEquals(words.get(12), "!");
		Assert.assertEquals(words.get(13), " ");
		Assert.assertEquals(words.get(14), "R");
		Assert.assertEquals(words.get(15), "?");
	}
}
