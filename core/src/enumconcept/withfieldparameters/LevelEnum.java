package enumconcept.withfieldparameters;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public enum LevelEnum {
    HIGH (3),
    MEDIUM(2),
    LOW(1);

    private final int levelCode;

    private LevelEnum(int levelCode) {
        this.levelCode = levelCode;
    }

    public int getLevelCode() {
        return this.levelCode;
    }



}
