package enumconcept.withfieldparameters;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class CupHasLevel {

    LevelEnum level;

    public CupHasLevel(LevelEnum level) {
        this.level = level;
    }

    public int getLevelInCode() {
        return level.getLevelCode();
    }

    public LevelEnum getLevel() {
        return level;
    }


}
