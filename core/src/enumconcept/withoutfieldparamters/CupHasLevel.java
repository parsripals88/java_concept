package enumconcept.withoutfieldparamters;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class CupHasLevel {

    LevelEnum level;

    public CupHasLevel(LevelEnum level) {
        this.level = level;
    }

    public String getLevelInStringInIFStatements() {
        if(level == LevelEnum.HIGH) {
            return "HIGH";
        } else if( level == LevelEnum.MEDIUM) {
            return "MEDIUM";
        } else if( level == LevelEnum.LOW) {
            return "LOW";
        } else {
            return "Not Available";
        }
    }

    public String getLevelInStringInSwitchStatements() {
        switch (level) {
            case HIGH   : return "HIGH";
            case MEDIUM : return "MEDIUM";
            case LOW    : return "LOW";
            default : return "Not Available";
        }
    }

    public void setLevel(String level) {
        this.level = LevelEnum.getLevelBasedOnCode(level);
    }

    public String iterate() {
        StringBuilder sb = new StringBuilder();
        for (LevelEnum level : LevelEnum.values()) {
            sb.append(level);
        }
        return sb.toString();
    }




}
