package enumconcept.withoutfieldparamters;

/**
 * Created by Palaniappan on 9/23/2016.
 */

/*
Java enums extend the java.lang.Enum class implicitly, so your enum types cannot extend another class.

If a Java enum contains fields and methods, the definition of
fields and methods must always come after the list of constants in the enum.
Additionally, the list of enum constants must be terminated by a semicolon;
 */

public enum LevelEnum {
    HIGH,
    MEDIUM,
    LOW;

    public static LevelEnum getLevelBasedOnCode(String level) {
        return LevelEnum.valueOf(level);
    }

}
