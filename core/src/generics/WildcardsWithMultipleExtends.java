package generics;

/**
 * Created by Palaniappan on 10/9/2016.
 */
/*
   extends should begin with classes and can be followed by interfaces
   Number is a class
   Serializable is an interface
   Comparable is an interface
   will get a compile error when started with interfaces
 */
public class WildcardsWithMultipleExtends
        <T extends Number & java.io.Serializable & Comparable<Number>> {
}
