package generics;

/**
 * Created by Palaniappan on 10/7/2016.
 */
public class ClassWithGenericMultipleParameters<K,V> {

    private K key;
    private V value;

    public ClassWithGenericMultipleParameters(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(K key) { this.key = key; }
    public void setValue(V value) { this.value = value; }
    public K getKey()   { return key; }
    public V getValue() { return value; }

}

/*
    Class can be initialized as
    Pair<String, Integer> p1 = new ClassWithGenericMultipleParameters<String, Integer>("Even", 8);
    Pair<String, String>  p2 = new ClassWithGenericMultipleParameters<String, String>("hello", "world");
 */
