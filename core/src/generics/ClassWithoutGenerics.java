package generics;

/**
 * Created by Palaniappan on 10/7/2016.
 */
public class ClassWithoutGenerics {
    private Object object;

    public void setObject(Object object){
        this.object = object;
    }

    public Object get() {
        return object;
    }
}
