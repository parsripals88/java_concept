package generics;

/**
 * Created by Palaniappan on 10/7/2016.
 */
public class RawTypes {

    public void rawInitialization() {
        //without any generic type
        //i.e. ClassWithGeneric<String> rawBox = new ClassWithGeneric<String>();
        ClassWithGeneric rawBox = new ClassWithGeneric();
    }

    public void typeCastingValidFromSpecificTypeToRawType() {
        ClassWithGeneric<String> genericBox = new ClassWithGeneric<>();
        ClassWithGeneric rawBox = genericBox;
    }

    public void rawTypeUsed() {
        // warning: unchecked invocation to set(T)
        ClassWithGeneric<String> genericBox = new ClassWithGeneric<>();
        ClassWithGeneric rawBox = genericBox;
        rawBox.set(8);
    }

    public void typeCastingInValidFromRawTypeToSpecificType() {
        //warning: unchecked conversion
        /*
        Note: RawTypes.java uses unchecked or unsafe operations.
        Note: Recompile with -Xlint:unchecked for details.
        we can use @SuppressWarnings to ignore these warnings
         */
        ClassWithGeneric rawBox = new ClassWithGeneric();
        ClassWithGeneric<String> genericBox = rawBox;
    }




}
