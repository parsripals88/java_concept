package generics;

/**
 * Created by Palaniappan on 10/9/2016.
 */
public class WildcardsWithExtends<T> {

    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get() {
        return t;
    }

    public <U extends Number> void inspect(U u) {
        System.out.println("T: " + t.getClass().getName());
        System.out.println("U: " + u.getClass().getName());

        //you can use the methods such as intValue of Number type
        int n = u.intValue() % 2;
    }

    public static void main(String[] args) {
        WildcardsWithExtends<Integer> integerBox = new WildcardsWithExtends<Integer>();
        integerBox.set(new Integer(10));
        //error: this is still String! because it should extend Number
        //integerBox.inspect("some text");
    }

    /************************************************************************/

    public static <T> int countGreaterThan(T[] anArray, T elem) {
        int count = 0;
        for (T e : anArray)
            // compiler error since greater than operator (>) applies
            // only to primitive types such as short, int, double, long, float, byte, and char.
            // You cannot use the > operator to compare objects.
            //if (e > elem)
                ++count;
        return count;
    }

    //Fix for above method is to use compare method
    public static <T extends Comparable<T>> int countGreaterThan(T[] anArray, T elem) {
        int count = 0;
        for (T e : anArray)
            if (e.compareTo(elem) > 0)
                ++count;
        return count;
    }
}
