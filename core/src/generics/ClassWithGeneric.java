package generics;

/**
 * Created by Palaniappan on 10/7/2016.
 */
public class ClassWithGeneric<T> {
    //T stands for type
    //A type variable can be any non-primitive type you specify:
    // any class type, any interface type, any array type,
    // or even another type variable.

    /*
        ClassWithGeneric<T> where T is the type parameter
        ClassWithGeneric<Integer> where Integer is the type argument
     */
    private T t;

    public void set(T t) {
        this.t = t;
    }

    public T get(){
        return t;
    }

    /*
    valid instantiation of the type parameter
    Box<Integer> integerBox = new Box<>();
     */

    /*Type conventions
    E - Element (used extensively by the Java Collections Framework)
K - Key
N - Number
T - Type
V - Value
S,U,V etc. - 2nd, 3rd, 4th types
     */
}
