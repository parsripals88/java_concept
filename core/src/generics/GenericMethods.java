package generics;

/**
 * Created by Palaniappan on 10/9/2016.
 */
public class GenericMethods {

    public static <K,V> boolean compare(
            ClassWithGenericMultipleParameters<K,V> p1,
            ClassWithGenericMultipleParameters<K,V> p2){
        return p1.getKey().equals(p2.getKey()) &&
                p1.getValue().equals(p2.getValue());
    }


}
