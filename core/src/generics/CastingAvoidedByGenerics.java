package generics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Palaniappan on 10/9/2016.
 */
public class CastingAvoidedByGenerics {

    public void castingExample() {
        List list = new ArrayList();
        list.add("hello");
        String s = (String)list.get(0);
    }

    public void castingAvoidedByGenerics() {
        List<String> list = new ArrayList<String>();
        list.add("hello");
        String s = list.get(0);   // no cast
    }

}
