package variables;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

public class ClassVariable {

	int myClassVariable;
	
	@Test
	public void classVariableAssert() {
		//class core.variables are stored in heap
		//when not initialized will used the default value
		//heap is garbage collected
		Assert.assertEquals(myClassVariable, 0);
	}
	
}
