package variables;

import org.junit.Test;

public class localvariable {
	
	@Test
	public void myMethod() {
		//local core.variables are declared in stack
		//needs to be initialized before use
		//stack core.variables are deleted when the scope ends
		int myLocalVariable = 25;
	}
}
