package exception;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CheckedException {

	@Test
	public void checkArithmeticException() {
		try {
			check(5);
		} catch(Exception e) {
			Assert.assertEquals(e.getMessage(), "it is an odd number");
		} finally {
			System.out.println("finally");
		}
	}
	
	public String check(int number) throws Exception {
		if(number % 2 == 0) {
			return "it is an even number";
		} else {
			throw new Exception("it is an odd number");
		}
	}
}
