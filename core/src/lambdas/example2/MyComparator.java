package lambdas.example2;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public interface MyComparator {

    public boolean compare(int a1, int a2);
}
