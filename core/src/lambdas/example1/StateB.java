package lambdas.example1;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class StateB implements State {
    @Override
    public String on() {
        return "B is on";
    }

    @Override
    public String off() {
        return "B is off";
    }

    @Override
    public String ack() {
        return "B is ack";
    }
}
