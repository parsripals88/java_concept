package lambdas.example1;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public interface StateChangeListener {

    String onStateChange(State oldState, State newState);

}
