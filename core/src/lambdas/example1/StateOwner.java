package lambdas.example1;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class StateOwner {

    StateChangeListener listener;

    public void addStateListener(StateChangeListener listener) {
        this.listener = listener;
    }

    public StateChangeListener getJava7DefaultStateListener() {
        return new StateChangeListener() {

            @Override
            public String onStateChange(State oldState, State newState) {
                return oldState.off() + newState.on();
            }
        };
    }

    /*
    functional interface is an interface that has just one abstract method, and thus represents a single function contract.
    lambda can be implemented for functional interface alone
    From the return type, what interface to map is identified
     */
    public StateChangeListener getJava8DefaultStateListener() {
        //zero parameter
//        () -> System.out.println("Zero parameter lambda");

        //one parameter
//        return (param) -> System.out.println("One parameter: " + param);


        //two parameters as an expression
        //TODO: not working nees to fix
//        (oldState, newState) -> {
//            System.out.println("Lambda learning");
//            return "off A and on B";
//        };

        return (State oldState, State newState) -> oldState.off() + newState.on();
    }
}
