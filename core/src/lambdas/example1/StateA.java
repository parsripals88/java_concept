package lambdas.example1;

/**
 * Created by Palaniappan on 9/23/2016.
 */
class StateA implements State {
    @Override
    public String on() {
        return "A is on";
    }

    @Override
    public String off() {
        return "A is off";
    }

    @Override
    public String ack() {
        return "A is ack";
    }
}
