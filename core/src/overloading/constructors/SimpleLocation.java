package overloading.constructors;

/** 
 * A class to illustrate class design and use.
 * Used in module 2 of the UC San Diego MOOC Object Oriented Programming in Java
 * 
 * @author UC San Diego Intermediate Software Development MOOC team
 * 
 *
 */
public class SimpleLocation
{
    public double latitude;
    public double longitude;
    
    //default constructors
    public SimpleLocation()
    {
        this.latitude = 32.9;
        this.longitude = -117.2;
    }
   
    //parameter core.constructor overloaded with
    public SimpleLocation(double latIn, double lonIn)
    {
        this.latitude = latIn;
        this.longitude = lonIn;
    }
   
}
