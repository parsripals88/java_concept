package encapsulation;

public class EncapsulationClass {

	//Encapsulation is also called data hiding
	//Process of hiding the members from outside the class
	//Implemented using the concept of access specifiers
		//public : access anywhere
		//private : only inside class
		//default : (not specified) in same package any class
		//protected : same package + child class outside package
	
	//Data member are usually kept private
	//The methods which expose the behaviour are kept public
	private int age;
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		if(age<0) System.out.println("Invalid Age");
		else this.age = age;
	}
}
