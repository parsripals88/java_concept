package encapsulation.outsidepackage;

import encapsulation.protectedaccess.PresentIn;

public class protectedaccess {
	public void accessor(){
		PresentIn present = new PresentIn();
		//present.latitude = 100.0; 
		//can access protected specifiers within the same package
	}
}
