package encapsulation.protectedaccess;

public class SubClassAccessedBy extends PresentIn{

	public void accessor(){
		PresentIn present = new PresentIn();
		present.latitude = 100.0; 
		//can access protected specifiers if I extend the subclass
	}
}
