package encapsulation.protectedaccess;

public class AccessedBy {

	public void accessor(){
		PresentIn present = new PresentIn();
		present.latitude = 100.0; 
		//can access protected specifiers within the same package
	}
}
