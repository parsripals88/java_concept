package encapsulation.publicaccess;

public class AccessedBy {

	public void accessor(){
		PresentIn present = new PresentIn();
		present.latitude = 100.0; //can access public specifiers anywhere
	}
}
