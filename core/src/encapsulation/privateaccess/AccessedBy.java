package encapsulation.privateaccess;

public class AccessedBy {

	public void accessor(){
		PresentIn present = new PresentIn();
		//present.latitude = 100.0; 
		//will throw an error if I try to access a private specifier
	}
}
