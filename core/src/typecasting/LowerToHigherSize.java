package typecasting;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class LowerToHigherSize {
	//core.variables of smaller size can go into bigger size\
	//byte can go into short, byte
	//short can go into int
	//byte --> short --> int --> long --> float --> double
	
	@Test
	public void conversion() {
		byte b = 10;
		int i = b;
		Assert.assertEquals(b, i);
	}
	
	@Test
	public void intToFloatConversion() {
		int k = 10;
		float fk = k;
		Assert.assertEquals(k, fk, 0);
	}
}
