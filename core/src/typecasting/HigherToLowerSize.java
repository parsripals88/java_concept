package typecasting;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

public class HigherToLowerSize {
   //larger size cannot go into a smaller size
   // int can't go into byte
   // need explicit type casting
	
	@Test
	public void conversion() {
		int i = 10;
		byte b = (byte) i;
		Assert.assertEquals(i, b);
	}
	
	@Test
	public void floatToByteConversion() {
		float f = 10.53f;
		// you need to write the symbol f otherwise by default assumes as float 
		byte i = (byte)f;
		Assert.assertEquals(f, i, 0.53);
	}
	
	@Test
	public void intToCharConversion() {
		int i = 65;
		char c = (char)i; //A
		Assert.assertEquals(i, c);
	}
	
	@Test
	public void intDivision() {
		int a = 10;
		int b = 3;
		//arithmetic operation is always int
		float div = a/b;
		Assert.assertEquals(div, 3, 0);
		
		div = (float)a/b;
		Assert.assertEquals(div, 3.33, 0.01);
	}
}
