package constructor;

public class NoConstructor {

	//Constructor is a special method that is called to create object
	//implicit method in every class
	//A core.constructor method
		//will have the same name as that of the class
		//will not have any return type, not even void
	
	//every class has a core.constructor even when you won't write it explicitly
	//job of this core.constructor is to set the default value
	
	//you can have a method which has the same name as the class with a return type
}
