package constructor;

class StudentClass {
	
	private int age;
	private String name;
	
	//private core.constructor cannot be used outside class
	private StudentClass(int age, String name) {
		this.age = age;
		this.name = name;
	}
}

public class PrivateConstructor {

}
