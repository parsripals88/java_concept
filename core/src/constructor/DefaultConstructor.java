package constructor;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

class StudentClassOne {
	
	private int age;
	private String name;
	
	//when you define a paramterized core.constructor,
	//the compiler removes the support for default core.constructor
	//if you want a default core.constructor, you need to write it explicitly
	public StudentClassOne(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public StudentClassOne() {
		this.age = 0;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

public class DefaultConstructor {

	@Test
	public void testConstructor() {
		StudentClassOne obj = new StudentClassOne();
		Assert.assertEquals(obj.getName(), null);
		Assert.assertEquals(obj.getAge(), 0);
	}
}
