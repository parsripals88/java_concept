package constructor;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

class Student {
	
	private int age;
	private String name;
	
	//we can create user defined core.constructor to initialize the data
	public Student(int age, String name) {
		this.age = age;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

public class ParameterizedConstructor {

	@Test
	public void testConstructor() {
		Student obj = new Student(25, "name");
		obj.setAge(26);
		Assert.assertEquals(obj.getName(), "name");
		Assert.assertEquals(obj.getAge(), 26);
	}
}
