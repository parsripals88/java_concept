package memory;

class sampleObject{
	String name = "xyz";
	int age;
}

public class stackvsheap {
	public static void main(String[] args) {
		int local = 10;
		sampleObject obj = new sampleObject();
	}
	
	//Memory Structure
	
	//Stack Memory        Heap Memory
	//obj Address --->    name: xyz age : 0
	//local : 10
}
