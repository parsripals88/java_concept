package inheritance;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class Starter {

	@Test
	public void testInheritance() {
		ChildMan man = new ChildMan();
		man.name = "Xyz";
		man.game = "Cricket";
		man.job = "Educator";
		Assert.assertEquals(man.work(), "Child Man Works");
		assertEquals(man.play(), "Child Kid Plays");
		assertEquals(man.eat(), "Parent Human Eats");
	}
}
