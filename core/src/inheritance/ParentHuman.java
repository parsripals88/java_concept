package inheritance;

public class ParentHuman {
	
	public String name;
	
	public String eat() {
		return "Parent Human Eats";
	}
	
}
