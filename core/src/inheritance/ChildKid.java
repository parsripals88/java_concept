package inheritance;

public class ChildKid extends ParentHuman{
	//Accessing fields and methods from another class by extending that class
	//extends is the keyword used to inherit the properties of a class
	//the class which inherits the properties of other is known as subclass(derived class, child class)
	//the class whose properties are inherited is know as superclass (base class, parent class)
	public String game;
	
	public String play() {
		return "Child Kid Plays";
	}
	
}
