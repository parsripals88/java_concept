package keywords.thiskeyword;

public class ThisVariable {

private int age;
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		if(age<0) System.out.println("Invalid Age");
		//this keyword will help to refer to the object that invoked the method
		else this.age = age;
	}
}
