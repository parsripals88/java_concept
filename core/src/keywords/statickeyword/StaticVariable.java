package keywords.statickeyword;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

class Student {
	
	private int studentId;
	
	//static data is a data that is common to the entire class
	//use to share data between object
	//non static data comes alive only when object is created
	private static int count = 0;
	
	public Student() {
		this.count = count + 1;
		this.studentId = 1000 + count;
	}

	public int getStudentId() {
		return studentId;
	}
	
}

public class StaticVariable {

	@Test
	public void testStaticVariable() {
		Student objOne = new Student();
		Assert.assertEquals(objOne.getStudentId(), 1001);
		Student objTwo = new Student();
		Assert.assertEquals(objTwo.getStudentId(), 1002);
	}
	
	
}
