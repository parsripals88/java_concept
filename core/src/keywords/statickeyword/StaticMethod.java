package keywords.statickeyword;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

class StudentClass {
	
	private int studentId;
	
	//static method is a method that is common to the entire class
	//static method can access only other static data and methods
	private static int count = 0;
	
	public StudentClass() {
		this.count = count + 1;
		this.studentId = 1000 + count;
	}

	public int getStudentId() {
		//can access static data from a non-static method
		return studentId;
	}
	
	public static int getCount() {
		//cannot access non-static data from a static method
		return count;
	}
}

public class StaticMethod {

	@Test
	public void testStaticVariable() {
		StudentClass objOne = new StudentClass();
		Assert.assertEquals(StudentClass.getCount(), 1);
		StudentClass objTwo = new StudentClass();
		Assert.assertEquals(StudentClass.getCount(), 2);
	}
}
