package keywords.statickeyword;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

class StudentTemp {
	
	private int studentId;
	
	private static int count = 0;
	
	//used to initialize the static data members
	//Executed when a class is first loaded	
	//static block is called before core.constructor call
	//static block is called only once when the class is loaded
	static {
		count = 100;
	}

	public int getStudentId() {
		return studentId;
	}	
	
	public static void setCount(int count) {
		StudentTemp.count = count;
	}

	public static int getCount() {
		return count;
	}
}

public class StaticBlock {

	@Test
	public void testStaticVariable() {
		StudentTemp objOne = new StudentTemp();
		Assert.assertEquals(StudentTemp.getCount(), 100);
		StudentTemp.setCount(200);
		Assert.assertEquals(StudentTemp.getCount(), 200);
	}
	
}
