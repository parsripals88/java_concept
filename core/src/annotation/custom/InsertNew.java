package annotation.custom;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public @interface InsertNew {

    String value();

}
