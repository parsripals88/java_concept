package annotation.custom;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public @interface Entity {

    String value() default "";
    String tableName();
    String primaryKey();
    int      age();
    String[] newNames();
}
