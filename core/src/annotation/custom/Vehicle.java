package annotation.custom;

/**
 * Created by Palaniappan on 9/23/2016.
 */
@Entity(tableName = "vehicles",
        primaryKey = "id",
        age=37,
        newNames={"Jenkov", "Peterson"})
public class Vehicle {

    @InsertNew("yes")
    protected String vehicleName = null;

    @Getter
    public String getVehicleName() {
        return this.vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }
}
