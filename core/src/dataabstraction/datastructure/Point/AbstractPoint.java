package dataabstraction.datastructure.Point;

/*
 * reveals no information about implementation
 * could be rectangular or polar co-ordinates
 */
public interface AbstractPoint {
	double getX();
	double getY();
	void setCartesian(double x, double y);
	double getR();
	double getTheta();
	void setPolar(double r, double theta);
}
