package dataabstraction.datastructure.Point;

/*
 * reveals more information about the data
 */
public class ConcretePoint {
	public double x;
	public double y;
}
