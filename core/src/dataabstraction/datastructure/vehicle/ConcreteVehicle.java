package dataabstraction.datastructure.vehicle;

/*
 * clearly exposes the internal implementation
 */
public interface ConcreteVehicle {
	double getFuelTankCapacityInGallons();
	double getGallonsOfGasoline();
}
