package dataabstraction.datastructure.vehicle;

public interface AbstractVehicle {
	double getPercentFuelRemaining();
}
