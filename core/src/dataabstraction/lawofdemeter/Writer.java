package dataabstraction.lawofdemeter;



public class Writer {
	
	Context ctxt;

	public void getOutputDir() {
		/*
		 * wrong since it tries to talk to other classes which it isn't aware of
		 * Method f of a class c should only talk to
		 * 	 C
		 *   An object created by f
		 *   An object passed as an argument to f
		 *   An object held in an instance variable of C
		 */
		ctxt = new Context();
		final String outputDir = ctxt.getOptions().getScratchDir().getAbsolutePath();

	}

}
