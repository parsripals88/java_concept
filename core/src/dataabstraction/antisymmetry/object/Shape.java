package dataabstraction.antisymmetry.object;

/*
 * Objects hide their data structures and have no meaningful functions.
 * OO code makes it easy to add new classes without changing existing functions
 * Add new data types is easier here
 */
public interface Shape {
	public double area();
}
