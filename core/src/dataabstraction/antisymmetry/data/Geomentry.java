package dataabstraction.antisymmetry.data;

/*
 * Data structures expose their data and have no meaningful functions
 * Procedural code makes it easy to add new classes without changing existing functions
 * Add new functions is easier here
 */
public class Geomentry {
	public final double PI = 3.14159265358793;
	
	public double area(Object shape) throws NoSuchShapeExtension {
		if(shape instanceof Square) {
			Square s = (Square)shape;
			return s.side * s.side;
		}  else if (shape instanceof Rectangle) {
			Rectangle r = (Rectangle)shape;
			return r.height * r.width;
		} else if (shape instanceof Circle) {
			Circle c = (Circle)shape;
			return PI * c.radius * c.radius;
		}
		throw new NoSuchShapeExtension();
	}
}
