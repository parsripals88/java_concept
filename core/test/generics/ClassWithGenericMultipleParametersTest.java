package generics;

/**
 * Created by Palaniappan on 10/9/2016.
 */
public class ClassWithGenericMultipleParametersTest {

    public void initializationTest() {
        ClassWithGenericMultipleParameters<String, Integer> p1 =
                new ClassWithGenericMultipleParameters<String, Integer>("Even", 8);
        ClassWithGenericMultipleParameters<String, String>  p2 =
                new ClassWithGenericMultipleParameters<String,String>("hello", "world");
    }

    public void initializationTestComplexTypesTest() {
        ClassWithGenericMultipleParameters<String, ClassWithGeneric<Integer>> p =
                new ClassWithGenericMultipleParameters<>("primes", new ClassWithGeneric<Integer>());
    }

}