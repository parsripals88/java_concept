package generics;

import org.junit.Test;

/**
 * Created by Palaniappan on 10/9/2016.
 */
public class GenericMethodsTest {

    @Test
    public void compareTest() throws Exception {
        ClassWithGenericMultipleParameters<Integer, String> p1
                = new ClassWithGenericMultipleParameters<>(1, "apple");
        ClassWithGenericMultipleParameters<Integer, String> p2
                = new ClassWithGenericMultipleParameters<>(2, "pear");
        boolean same = GenericMethods.<Integer, String>compare(p1, p2);
    }

    @Test
    public void compareWithCompileAutomaticallyInferringTypes() throws Exception {
        ClassWithGenericMultipleParameters<Integer, String> p1
                = new ClassWithGenericMultipleParameters<>(1, "apple");
        ClassWithGenericMultipleParameters<Integer, String> p2
                = new ClassWithGenericMultipleParameters<>(2, "pear");
        boolean same = GenericMethods.compare(p1, p2);
    }

}