package autoboxing;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class ObjectToPrimitiveTest {

    @Test
    public void integerToint() throws Exception {
        ObjectToPrimitive otp = new ObjectToPrimitive();
        Assert.assertEquals(9, otp.IntegerToint(9));
    }

}