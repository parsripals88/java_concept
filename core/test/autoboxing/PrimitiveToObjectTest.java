package autoboxing;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Palaniappan on 9/23/2016.
 */
public class PrimitiveToObjectTest {

    @Test
    public void intToInteger() throws Exception {
        PrimitiveToObject pto = new PrimitiveToObject();
        Assert.assertEquals(new Integer(9), (Integer)pto.intToInteger(9));
    }

}