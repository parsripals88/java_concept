package primitiveclass;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Created by Palaniappan on 11/18/2016.
 */
public class LinkedListAsDequeClass {
    
    Deque<Integer> list;
    
    @Before
    public void setUp() {
        list = new LinkedList<>();
        for(int i=0; i<=5; i++)
            list.addFirst(i);
    }
    
    @Test
    public void addFirstTest() {
        list.addFirst(6);
        Assert.assertEquals(6, (int)list.peek());
    }

    @Test
    public void peekTest() {
        Assert.assertEquals(5, (int)list.peek());
    }

    @Test
    public void pollTest(){
        Assert.assertEquals(5, (int)list.poll());
        Assert.assertEquals(4, (int)list.poll());
        Assert.assertEquals(3, (int)list.poll());
        Assert.assertEquals(2, (int)list.poll());
        Assert.assertEquals(1, (int)list.poll());
        Assert.assertEquals(0, (int)list.poll());
        Assert.assertEquals(null, list.poll());
    }

    @Test
    public void popTest() {
        Assert.assertEquals(5, (int)list.pop());
        Assert.assertEquals(4, (int)list.pop());
        Assert.assertEquals(3, (int)list.pop());
        Assert.assertEquals(2, (int)list.pop());
        Assert.assertEquals(1, (int)list.pop());
        Assert.assertEquals(0, (int)list.pop());
        try {
            list.pop();
        } catch(NoSuchElementException e) {

        }
        Assert.assertEquals(null, list.peekFirst());
        Assert.assertEquals(null, list.peekLast());
    }

    @Test
    public void isEmptyTest() {
        Assert.assertEquals(false, list.isEmpty());
        for(int i=0;i<=5;i++) {
            list.poll();
        }
        Assert.assertEquals(true, list.isEmpty());
    }

    @Test
    public void peekFirstTest() {
        Assert.assertEquals(5, (int)list.peekFirst());
    }

    @Test
    public void peekLastTest() {
        Assert.assertEquals(0, (int)list.peekLast());
    }

    @Test
    public void removeFirstTest() {
        Assert.assertEquals(5, (int)list.removeFirst());
        Assert.assertEquals(4, (int)list.removeFirst());
        Assert.assertEquals(3, (int)list.removeFirst());
        Assert.assertEquals(2, (int)list.removeFirst());
        Assert.assertEquals(1, (int)list.removeFirst());
        Assert.assertEquals(0, (int)list.removeFirst());
        try {
            list.removeFirst();
        } catch(NoSuchElementException e) {

        }
    }

    @Test
    public void descendingIteratorTest() {
        Iterator<Integer> iterator = list.descendingIterator();
        for(int i=0;i<=5;i++) {
            Assert.assertEquals(i, (int)iterator.next());
        }
    }

    @Test
    public void iteratorTest() {
        Iterator<Integer> iterator = list.iterator();
        for(int i=5;i>=0;i--) {
            Assert.assertEquals(i, (int)iterator.next());
        }
    }


}
