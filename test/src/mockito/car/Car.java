package mockito.car;

/**
 * Created by Palaniappan on 9/25/2016.
 */
public interface Car {

    boolean needsFuel();
    double getEngineTemperature();
    void driveTo(String destination);
}
